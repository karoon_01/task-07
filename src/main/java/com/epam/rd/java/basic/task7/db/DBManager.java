package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;

	private String getConnectionUrl(){
		Properties props = new Properties();
		try(InputStream stream = Files.newInputStream(Paths.get("app.properties"))){
			props.load(stream);
		} catch(IOException ex){
			ex.printStackTrace();
		}
		return props.getProperty("connection.url");
	}

	public static synchronized DBManager getInstance(){
		try{
			if(instance == null) instance = new DBManager();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return instance;
	}

	private DBManager() throws Exception {
	}

	public List<User> findAllUsers() throws DBException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<User> list = new ArrayList<>();
		String query = "select * from users";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			while(resultSet.next()){
				list.add(User.createUser(resultSet.getString(2)));
			}
		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
			return list;
		}
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		boolean res = false;

		String query = "insert into users values (DEFAULT, ?)";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getLogin());

			if (statement.executeUpdate() > 0) {
				rs = statement.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					user.setId(id);
					res = true;
				}
			}
		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}

		return res;
	}

	public boolean deleteUsers(User... users) throws DBException {

		Connection connection = null;
		PreparedStatement statement = null;

		List<String> list = List.of(users).stream().map(User::getLogin).collect(Collectors.toList());
		String query = "delete from users where login = ?";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			for(String str: list){
				statement.setString(1, str);
				statement.execute();
			}

		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		User user = new User();
		String query = "select * from users where login = ?";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			statement.setString(1, login);

			resultSet = statement.executeQuery();
			while(resultSet.next()){
				user.setId(resultSet.getInt(1));
				user.setLogin(resultSet.getString(2));
			}

		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		Team team = new Team();
		String query = "select * from teams where name = ?";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			statement.setString(1, name);

			resultSet = statement.executeQuery();
			while(resultSet.next()){
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
			}

		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<Team> list = new ArrayList<>();
		String query = "select * from teams";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);

			while(resultSet.next()){
				list.add(Team.createTeam(resultSet.getString(2)));
			}
		} catch (SQLException e){
			throw new DBException("Error", e);
		}

		return list;

	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		boolean res = false;
		String query = "insert into teams values (DEFAULT, ?)";


		try{
		connection = DriverManager.getConnection(getConnectionUrl());
		statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			statement.setString(k, team.getName());
			if (statement.executeUpdate() > 0) {
				rs = statement.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					team.setId(id);
					res = true;
				}
			}

		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}
		return res;

	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		String query = "insert into users_teams (user_id, team_id)" + "values (?, ?)";
		List<Integer> list = List.of(teams).stream().map(Team::getId).collect(Collectors.toList());

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			connection.setAutoCommit(false);

			for(Integer i: list){
				statement.setInt(1, user.getId());
				statement.setInt(2, i);
				statement.executeUpdate();
			}
			connection.commit();
		} catch(SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException("Rollback", ex);
			}
			throw new DBException("Error", e);
		}
		finally {
			try {
				connection.close();
				statement.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<Team> list = new ArrayList<>();
		List<Integer> listId = new ArrayList<>();

		String query = "select team_id from users_teams where user_id = ?";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			statement.setInt(1, user.getId());
			resultSet = statement.executeQuery();

			while(resultSet.next()){
				listId.add(resultSet.getInt(1));
			}

			query = "select name from teams where id = ?";
			statement = connection.prepareStatement(query);
			for(Integer i : listId){
				statement.setInt(1, i);
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					list.add(Team.createTeam(resultSet.getString(1)));
				}
			}


		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}

		return list;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		String query = "delete from teams where name = ?";

		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			statement.setString(1, team.getName());
			statement.executeUpdate();
		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;

		String query = "update teams set name = ? where id = ?";
		try{
			connection = DriverManager.getConnection(getConnectionUrl());
			statement = connection.prepareStatement(query);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.execute();
		} catch (SQLException e){
			throw new DBException("Error", e);
		}finally {
			try {
				connection.close();
				statement.close();
			} catch (SQLException e) {
				throw new DBException("Error", e);
			}
		}

		return true;
	}

}
