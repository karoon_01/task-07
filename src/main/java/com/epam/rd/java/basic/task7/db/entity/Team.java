package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;
	private String name;

	@Override
	public boolean equals(Object obj) {

		if(obj == null) return false;
		if(obj.getClass() != this.getClass()) return false;
		Team team = (Team) obj;

		return this.getName().equals(team.getName());
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		return team;
	}

}
